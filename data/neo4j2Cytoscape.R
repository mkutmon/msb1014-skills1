## REQUIRED PACKAGES
## MAKE SURE THEY ARE ALL INSTALLED!
library(neo4r)
library(dplyr)
library(purrr)
library(RCy3)

##########################################
#### Setup Start - Run once at the beginning!
##########################################

con <- neo4j_api$new(
  # CHECK THE URL AND PASSWORD OF YOUR OWN NEO4J SANDBOX!
  url = "http://54.152.215.66:7474",
  user = "neo4j", 
  password = "chaplains-ends-indicate"
)

# function - converts Neo4j graph in iGraph - used as input for Cytoscape
getIGraph <- function(G) {
  G$nodes <- G$nodes %>%
    unnest_nodes(what = "properties") %>% 
    mutate(label = map_chr(label, 1))
  
  G$relationships <- G$relationships %>%
    unnest_relationships() %>%
    select(startNode, endNode, type, everything())
  
  graph_object <- igraph::graph_from_data_frame(
    d = G$relationships, 
    directed = TRUE, 
    vertices = G$nodes
  )
  return(graph_object)
} 

##########################################
#### SETUP END
##########################################

##########################################
#### CODE STARTS
##########################################

#adapt query if needed - this example works only for queries
#that return a graph (not number or table!)
query <- "MATCH (panic:Disease)-[r1]-(protein:Protein)-[r2]-(disease:Disease) WHERE panic.name = \"Panic disorder\" AND NOT (panic) = (disease) AND r1.confidence > 1 AND r2.confidence > 1 RETURN panic, protein, disease, r1, r2"
G <- query %>% call_neo4j(con, type = "graph") 

#convert from Neo4j to iGraph
igraph <- getIGraph(G)

#make sure Cytoscape is opened - Cytoscape network will be created
RCy3::createNetworkFromIgraph(igraph)
RCy3::copyVisualStyle("default","neo4j")
RCy3::setVisualStyle("neo4j")
RCy3::setNodeColorMapping(table.column = "label", table.column.values = c("Disease","Protein"), colors = c("#FF9900","#6699FF"), mapping.type = "d", style.name = "neo4j")

