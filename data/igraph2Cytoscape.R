if(!"RCy3" %in% installed.packages()){
  install.packages("BiocManager")
  BiocManager::install("RCy3")
}
if(!"igraph" %in% installed.packages()){
  install.packages("BiocManager")
  BiocManager::install("igraph")
}
library(RCy3)
library(igraph)

# install.packages("rstudioapi") # run this if it's your first time using it to install
library(rstudioapi) # load it
# the following line is for getting the path of your current open file
current_path <- getActiveDocumentContext()$path 
# The next line set the working directory to the relevant one:
setwd(dirname(current_path ))

############################################################################################
# Les mis network

dataSet <- read.table("lesmis.txt", header = TRUE, sep = "\t")

graph <- igraph::simplify(igraph::graph.data.frame(dataSet, directed=FALSE))

# Print number of nodes and edges
vn <- igraph::vcount(graph)
en <- igraph::ecount(graph)

# Calculate degree for all nodes
degAll <- igraph::degree(graph, v = igraph::V(graph), mode = "all")
# Add degree as node property to graph
graph <- igraph::set.vertex.attribute(graph, "idegree", index = igraph::V(graph), value = degAll)

# plot degree distribution
d <- density(degAll) # returns the density data 
plot(d)

# Calculate local cluster coefficient
clustcoeff <- igraph::transitivity(graph, type = "local", v = igraph::V(graph))
clustcoeff[is.na(clustcoeff)] <- 0

# Add degree as node property to graph
graph <- igraph::set.vertex.attribute(graph, "iclustercoeff", index = igraph::V(graph), value = clustcoeff)

# plot cluster coefficient distribution
dc <- density(clustcoeff) # returns the density data 
plot(dc, xlim=c(0, 1))

RCy3::createNetworkFromIgraph(graph, title="Les mis", collection="igraph")

############################################################################################
# Change network visualization

RCy3::setNodeShapeDefault('ellipse')
RCy3::setNodeSizeDefault(20)
RCy3::setNodeSizeMapping('idegree', c(min(degAll), round(max(degAll)*0.2), max(degAll)), c(10,30,100))
RCy3::setNodeColorMapping('iclustercoeff', c(0,0.5,1), c('#92c0da', '#fefebe', '#fc8f5b'))

############################################################################################
# Random network comparison

# calculate max number of links
emax <- vn*(vn-1)/2

# calculate edge probability
prop <- en/emax

# create random graph with same number of nodes and links (approximately)
gRandom <- igraph::random.graph.game(n = vn, p.or.m = prop)

# Calculate degree for all nodes
degRandom <- igraph::degree(gRandom, v = igraph::V(gRandom), mode = "all")

# Add degree as node property to graph
gRandom <- igraph::set.vertex.attribute(gRandom, "idegree", index = igraph::V(gRandom), value = degRandom)

# plot degree distribution
dRandom <- density(degRandom) # returns the density data 
plot(dRandom, xlim=c(0, max(dRandom$x))) # plots the results

# Calculate local cluster coefficient
ccRandom <- igraph::transitivity(gRandom, type = "local", v = igraph::V(gRandom))
ccRandom[is.na(ccRandom)] <- 0

# Add degree as node property to graph
gRandom <- igraph::set.vertex.attribute(gRandom, "iclustercoeff", index = igraph::V(gRandom), value = ccRandom)

# plot cluster coefficient distribution
dcRandom <- density(ccRandom) # returns the density data 
plot(dcRandom, xlim=c(0, 1))

RCy3::createNetworkFromIgraph(gRandom, title="Random network", collection="igraph")
