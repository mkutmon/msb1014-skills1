# Part A: Graph databases (Neo4j)

### A.1. Introduction to Neo4j
You will get a short introduction by the lecturer to get you started. There are many tutorials online that you can learn more about in the future. 
In your free time, you can also go through this quick [introduction](https://gitlab.com/mkutmon/msb1014-2020-skills1/-/raw/master/data/Neo4j-Intro.pdf?inline=true).


----

### A.2. Investigating human protein-protein interaction network

Go to your own sandbox (Blank Sandbox - Graph Data Science) and use the following two commands to load the human PPI network from STRING.

Generation PPI network

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU/export?format=csv&id=11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU&gid=741786809" AS csvLine
MERGE (protein1:Protein { id: csvLine.protein1 })
MERGE (protein2:Protein { id: csvLine.protein2 })
MERGE (protein1)-[:INTERACTS { score: toInteger(csvLine.combined_score )}]-(protein2)`

Add gene names to protein nodes

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU/export?format=csv&id=11vjCmZoTPhQszS9nZzgmQLQECDKiaIbrtW4CD8DCYbU&gid=361761440" AS csvLine
MERGE (p:Protein { id: csvLine.protein })
ON MATCH SET p.proteinName = csvLine.geneName`

Only PPIs with a very high confidence cut-off >= 999 are included and in this part, we will investigate the network structure and use two biological questions as examples to explore the network.

Investigate the network structure by displaying a random set of 100 proteins.

`MATCH (p) RETURN p LIMIT 100`

You can click on the nodes and see the protein identifier and gene name. When clicking on the edge you will see the confidence score. 

![alt text](images/neo4j-1.png "Figure 1")

----

> Question A.2.1: Basic network statistics 
- How many nodes are in the network?
- How many edges are in the network? 

The degree distribution is one of the key differences between random and real networks. Try to understand the
following query and run in on the PPI database:

`MATCH (p:Protein)-[r]-() WITH p AS nodes, count(DISTINCT r) AS degree RETURN degree, count(nodes) AS num_nodes ORDER BY degree asc`

> Question A.2.2: Degree distribution
- How does the degree distribution of the network look like? 
- Is the degree distribution as you would expect it? Why? 

----

**Interaction partners**

Using CDK1 as the central protein, we will study the direct neighborhood of the protein in the network. 

> Question A.2.3: CDK1 neighbors
- How many neighbors does CDK1 have?
- Can you visualize the neighborhood network in the Neo4j browser? 

----

### A.3. Investigating the human disease-gene network

Make sure Assignment 2 is completely finished. Then delete all nodes and links (`MATCH (n) DETACH DELETE n`). Use the following command to load the human disease-gene association network. Note: genes are stored as _Protein_ nodes in the graph database.

`LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/u/0/d/1_OAd_6_n1Y-K9mYrKDJOIgN9nY_ntxWe2QGB88mZlV4/export?format=csv&id=1_OAd_6_n1Y-K9mYrKDJOIgN9nY_ntxWe2QGB88mZlV4&gid=0" AS csvLine
MERGE (protein:Protein {id: csvLine.protein, name: csvLine.proteinName })
MERGE (disease:Disease {id: csvLine.disease, name: csvLine.diseaseName })
CREATE (protein)-[:ASSOCIATED_WITH {confidence : toFloat(csvLine.confidence)}]->(disease)`

In this part, we will investigate the network structure and use a biological question as examples to explore the network.

Investigate the network structure by displaying a random set of 100 high-confidence disease-gene associations.

`MATCH (d:Disease)-[r]-(p:Protein) WHERE r.confidence > 1 RETURN d,r,p LIMIT 100`

You can click on the nodes and see the protein identifier and gene name. When clicking on the edge you will see
the confidence score. 

> Question A.3.1: Basic network statistics
- How many diseases are in the network?
- How many proteins are in the network?
- How many disease-gene associations are in the network?
- How many disease-gene associations have a confidence > 1?
- Which protein has the most associated diseases? (try to understand the query!)
```MATCH (p:Protein)-->() 
RETURN p.name,count(*) as degree 
ORDER BY degree DESC LIMIT 10
```


> Question A.3.2: Panic disorder
- How many genes are associated with “Panic disorder” (association confidence > 1)? (try to understand the query!)
```MATCH (panic:Disease)-[r]-(protein:Protein)
WHERE panic.name = "Panic disorder" AND  r.confidence > 1
RETURN count(protein)```
- What other diseases are associated with the same genes (association confidence > 1)? (try to understand the query!)
```MATCH (panic:Disease)-[r1]-(protein:Protein)-[r2]-(disease:Disease)
WHERE panic.name = "Panic disorder" AND NOT (panic) = (disease) AND r1.confidence > 1 AND r2.confidence > 1
RETURN panic, protein, disease```

