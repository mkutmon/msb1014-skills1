# 2024/25 - MSB1014 - Network Biology

- **Author**: [Martina Summer-Kutmon](http://maastrichtuniversity.nl/martina.kutmon)
- **Last updated**: 2024-09-06

----

# Skills session 1 - Graph theory

In this skills session, you will learn:
- What is Neo4j and how does it work?
- How can I work with networks in R with igraph?
- How can I visualize and analyze networks in Cytoscape?
- How to connect the tools?

----

### [Part A: Graph databases (Neo4j)](NEO4J.md) 

- A.1. Introduction to Neo4j (quick intro by lecturer)
- A.2. Investigating the human PPI network
- A.3. Investigating the human disease-gene network

----

### [Part B: Network analysis in R (igraph)](IGRAPH.md)

- B.0. Installation instructions
- B.1. Networks in igraph
- B.2. Network and node descriptives
- B.3. Distances and paths

----

### [Part C: Cytoscape and RCy3](CYTOSCAPE.md)

- C.0. Installation instructions
- C.1. Network analysis and visualization
- C.2. Automation

----

### Part D: Work independently

Now you have time to look at the example networks you were asked to investigate during the tutorial. Use igraph, Cytoscape and/or Neo4j to explore the network structure of your assigned network. Make sure you calculate all the properties in the table (see case 1 document) and create a degree histogram. Do you see any differences in the results of the different tools? 


