# Part C: Cytoscape

## C.0 Installation instructions

- Download and install Cytoscape 3.10.2: https://cytoscape.org/
- Start Cytsocape and install the following apps (Apps > App Manager):
  - yFiles

---- 

## C.1 Network analysis and visualization
Import the Les Misérables network in Cytoscape. The nodes in the network are characters and the links represent
co-occurrences in the play. 

- File > Import > Network > File …
- Select the "[lesmis.txt](https://gitlab.com/mkutmon/msb1014-2020-skills1/-/raw/master/data/lesmis.txt?inline=false)" file.
- Check preview (first column is source node, second column is target node, count is edge attribute)

![alt text](images/cytoscape1.png "Figure 1")

- Click OK

Apply a layout that might be more intuitive to see the network structure:
- Layout > yFiles Organic Layout

> Question C.1.1: Basic network properties
- How many nodes are in the network?
- How many links are in the network?
- Is the network directed?
- Are there self-loops in the network?
- Is their one giant component or are there also unconnected nodes and/or components?

----

Besides the nodes and the links in the network, the input file also contained the number of co-occurrences of two
characters. We can visualize the count as the edge width in the network visualization. In the Styles tab, create a
continuous mapping for the Width property:

![alt text](images/cytoscape2.png "Figure 2")

> Question C.1.2: Edge weight
- Which two characters occur in most scenes together? 
- There is a triangle of central characters that share the highest co-occurrence. Which characters are those?

----

Next, we will caluclate the network properties to study the topology of the network. 
- Go to Tools > Analyze Network > Ok 
- Choose to treat the network as undirected! (co-occurrences do not have a direction)
- You can see the result in the side tab.

> Question C.1.3: Node degree
- What is the average node degree?
- How does the degree distribution look like (button at the bottom)? Compare it to the expected distributions for real and random networks. - you might need to install an app CyPlot for this now.
- What are the highest and lowest node degrees in the network? Which character has the highest number of interactions with other characters? (check the node table below the network. A column “Degree” has been added, which you can sort by clicking on the header name. 

----

NetworkAnalyzer also provides some information regarding the path lengths in the network. Based on the small
world phenomenon, we know that in real networks, we expect short average path length indicating an easily
negotiable network in which information or mass can be transported throughout the network efficiently and fast. 

> Question C.1.4: Path length
- What is the characteristic path length?
- In the node table, you will now see an icon that looks like a Histogram. Click on it and select the AverageShortestPath to be visualized as a e.g. histogram. How does the shortest path length distribution look like?
- How long is the network diameter and what does that actually mean?

----

Now let's visualize the node degree and cluster coefficient in the network. 
- Click on Style panel and try to create a visualization for the node size mapping for Degree and a node fill color mapping for the cluster coefficient. If you need help with how to define Styles, you can read up more details here: https://manual.cytoscape.org/en/stable/Styles.html.

> Question C.1.5: Clustering coefficient
- Make a screenshot of the your final visualization of the network.
- Can you identify any hub nodes (high degree = large node)?
- What can you see in regard to the cluster coefficient? What does it mean? 

---- 

## C.2 Automation in random

The RCy3 R-package allows you to automate your Cytoscape analysis and visualization in R. Make sure you have RStudio and Cytoscape open next to each other. When running the RCy3 part of the code, Cytoscape will update in real-time. 

- **Script**: [igraph2cytoscape.zip](https://gitlab.com/mkutmon/msb1014-2020-skills1/-/raw/master/data/igraph2cytoscape.zip):<br/> 
*Les mis network analysis repeated in R using igraph and loaded into Cytoscape (script also contains part about comparing the network with a random network)*
