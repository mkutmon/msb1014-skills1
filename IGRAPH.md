# Part B: igraph R package

### B.0. Installation instructions

* Tested with R version 4.4.1 and igraph R package version 2.0.3 (`BiocManager::install("igraph")`) 

```
BiocManager::install("igraph")
library(igraph)
```

----

### B.1. igraph tutorial

There is a fantastic tutorial online on performing network analysis with R and iGraph. Download the R script (only required section + data) from [here](https://gitlab.com/mkutmon/msb1014-2020-skills1/-/raw/master/data/igraph.zip?inline=false). 

You can find the full tutorial by Katherine Ognyanova (https://kateto.net/networks-r-igraph) with a [Github repo](https://github.com/kateto/R-igraph-Network-Workshop-NetSciX) with code and instructions! 

We will focus only on the following sections:
- (2) Networks in igraph 
- (6) Network and node descriptives
- (7) Distances and paths

You should now be able to create random, small-world and scale-free network models (2) with the algorithms explained in the lecture. Check the degree distribution and average path lengths (as explained in 6 and 7) and discuss the findings. 
